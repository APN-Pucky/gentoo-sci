#!/bin/bash
D=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $D
git checkout main
git pull
A=($(find . -name "*.ebuild" | grep "sci-physics"))
P=${A[$(($RANDOM % ${#A[@]}))]}
echo "random_build $(date)" >> $(dirname $P)/.build
BRANCH=$(basename $P)_$(date +"%m_%d_%Y")
git checkout -b $BRANCH
git add .
git commit -am "try $P"
git push -o merge_request.create -o merge_request.target=main -o merge_request.merge_when_pipeline_succeeds origin "$BRANCH"
git checkout main


